## A initial port of the Arduino LiquidCrystal library to Raspberry Pi.


![gif](https://gitlab.com/sputkin/LiquidCrystalPi/raw/master/gif/display.gif)


#### Necessary dependencies

* ##### WiringPi http://wiringpi.com/download-and-install/



##### For configuring connections see wiringpi Pin Numbering . For a complete documentation on the library, see the official documentation https://www.arduino.cc/en/Reference/LiquidCrystal .

